import random

class population (object):

        def __init__(self, size):

            self.size = size
            self.chromosomes = []
            count = 0

            while count < self.size: self.chromosomes.append(chromosome())
            count += 1

        def get_chromosomes(self): return self.chromosomes

class chromosome (object):

            def __init__(self):

                self.genes = []
                self.fitness = 0
                count = 0

                while count < len(target):
                    if random.random() >= 0.5: self.genes.append(1)
                    else: self.genes.append(0)
                    count += 1

            def get_genes(self): return self.genes

            def calculate_fitness(self):

                for index in range(len(self.genes)):
                    if self.genes[index] == target[index]: self.fitness += 1
                    return self.fitness

            def __str__(self): return self.genes

def main():

        population_size = 5
        global target
        target = [1,1,0,0,1,0,1,1,0,1]
        iterator = 0

        Population = population(population_size)
        for chromo in Population.get_chromosomes():
            print "Chrommosome #: ", iterator, " : ", chromo, " | Fitness: ", \
            chromo.calculate_fitness()
            iterator += 1

main()
